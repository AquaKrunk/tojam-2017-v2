﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerZeta : MonoBehaviour {

    void Start () {
        if (FindObjectOfType<MainMenuUI>() != null) {
            FindObjectOfType<MainMenuUI>().PlayersReady += GameStart;
        }

        if(FindObjectOfType<CutsceneManager>() != null) {
            FindObjectOfType<CutsceneManager>().Fin += LoadMainLevel;
        }
    }

    void GameStart() {
        FindObjectOfType<MainMenuUI>().PlayersReady -= GameStart;
        SceneManager.LoadScene("CutsceneIntermission");

    }

    void LoadMainLevel() {
        FindObjectOfType<CutsceneManager>().Fin -= LoadMainLevel;
        SceneManager.LoadScene("MainLevel");
    }
}
