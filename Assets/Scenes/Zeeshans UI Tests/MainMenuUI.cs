﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUI : MonoBehaviour {
    #region Variables
    public RectTransform pOneTapper;
    public RectTransform pTwoTapper;

    const float TAP_STRENGTH = 0.12f;
    const float DECAY_FACTOR = 0.99f;

    float tapsP1 = 0f;
    bool togglep1 = false;
    float scalePercentP1 = 0f;

    float tapsP2 = 0f;
    bool togglep2 = false;
    float scalePercentP2 = 0f;
    #endregion


    public delegate void PlayerReady();
    public event PlayerReady PlayersReady;


    private void Update() {
        if (Input.GetKey(KeyCode.S) && togglep1) {
            if (pOneTapper.localScale.y < 1.05f) {
                tapsP1 += TAP_STRENGTH;
                scalePercentP1 = tapsP1;

                pOneTapper.localScale = new Vector3(1f, Mathf.Clamp(tapsP1, 0f, 1.05f), 1f);
                togglep1 = !togglep1;
            }
        }
        else {
            tapsP1 *= DECAY_FACTOR;
            scalePercentP1 *= DECAY_FACTOR;
            pOneTapper.localScale = new Vector3(1f, scalePercentP1, 1f);
        }

        if (Input.GetKey(KeyCode.K) && togglep2) {
            if (pTwoTapper.localScale.y < 1.05f) {
                tapsP2 += TAP_STRENGTH;
                scalePercentP2 = tapsP2;

                pTwoTapper.localScale = new Vector3(1f, Mathf.Clamp(tapsP2, 0f, 1.05f), 1f);
                togglep2 = !togglep2;
            }
        }
        else {
            tapsP2 *= DECAY_FACTOR;
            scalePercentP2 *= DECAY_FACTOR;
            pTwoTapper.localScale = new Vector3(1f, scalePercentP2, 1f);
        }

        if (Input.GetKeyUp(KeyCode.S) && !togglep1) {
            togglep1 = !togglep1;
        }
        if (Input.GetKeyUp(KeyCode.K) && !togglep2) {
            togglep2 = !togglep2;
        }

        if (pOneTapper.localScale.y >= 1.05f && pTwoTapper.localScale.y >= 1.05f) {
            PlayersReady();
        }
    }


}
