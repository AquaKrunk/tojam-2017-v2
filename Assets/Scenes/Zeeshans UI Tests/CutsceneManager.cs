﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CutsceneManager : MonoBehaviour {

    public Text countdownDisplay;

    public Text countdownDisplay2;

    private string[] skipText = { "Skip?", "Press!", "Space!" };

    public Sprite[] currentImage;
    public Image cutsceneImageDisplay;

    public AnimationClip skipPendulumAnim;
    public AnimationClip reverseSkipPendulumAnim;
    public Animator skipPendulum;
    public Text skipmytext;

    public delegate void Cutscene();
    public event Cutscene Fin;

    [Range(0, 99)]
    public int cutsceneTime = 13;

    private int timeCounter = 0;
    private int cutsceneNumber = 0;



    void Start () {
        StartCoroutine(DisplayTime(cutsceneTime));
        StartCoroutine(Pendulate(cutsceneTime));

        StartCoroutine(SkipScene());
    }

    IEnumerator SkipScene() {
        while (true) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                SceneManager.LoadScene("MainLevel");
            }

            yield return null;
        }
    }

    IEnumerator DisplayTime(int cutsceneTime) {
        while(cutsceneTime > 0) {
            --cutsceneTime;
            countdownDisplay.text = cutsceneTime.ToString();

            countdownDisplay2.text = cutsceneTime.ToString();

            skipmytext.text = skipText[timeCounter == skipText.Length ? timeCounter = 0 : timeCounter++];

            if (cutsceneTime <= 4) { cutsceneNumber = 0; }
            else if (cutsceneTime > 4 && cutsceneTime <= 9) { cutsceneNumber = 1; }
            else if (cutsceneTime > 9 && cutsceneTime <= 13) { cutsceneNumber = 2; }

            cutsceneImageDisplay.sprite = currentImage[cutsceneNumber];

            yield return new WaitForSeconds(1f);
        }
        Fin();
    }

    IEnumerator Pendulate(int cutsceneTime) {
        while(cutsceneTime >= 0) {
            --cutsceneTime;

            if(cutsceneTime % 2 == 0) { skipPendulum.Play(skipPendulumAnim.name); }
            else { skipPendulum.Play(reverseSkipPendulumAnim.name); }

            yield return new WaitForSeconds(1f);
        }

    }
}
