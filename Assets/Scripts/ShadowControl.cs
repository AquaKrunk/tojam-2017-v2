﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowControl : MonoBehaviour {

	public GameObject Torso;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3(Torso.transform.position.x - Torso.transform.up.x, this.transform.position.y,this.transform.position.z);
	}
}
