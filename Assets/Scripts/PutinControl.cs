﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutinControl : MonoBehaviour {

	private float StartingY;

	// Use this for initialization
	void Start () {
		StartingY = this.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3(this.transform.position.x + 0.15f, StartingY + 0.9f*Mathf.Sin(this.transform.position.x), this.transform.position.z);
	}
}
