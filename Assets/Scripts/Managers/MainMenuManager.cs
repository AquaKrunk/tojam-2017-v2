﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

	private GameObject SelectedMenuItem;
	public GameObject PlayMenuItem;
	public GameObject InstructionsMenuItem;
	public GameObject UICanvas;
	public GameObject FullInstructions;

	// Use this for initialization
	void Start () {
		GameObject.Find("Player_One").GetComponent<SquatterController>().UnlockRig();
		GameObject.Find("Player_Two").GetComponent<SquatterController>().UnlockRig();
		this.gameObject.GetComponent<InputManager>().InitializeControls();
		this.gameObject.GetComponent<BottlePool>().InitializeBottlePool(20);
		SelectedMenuItem = PlayMenuItem;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.Space) && FullInstructions.activeSelf == false)
		{
			ToggleMenuItem();
		}
		if(Input.GetKeyDown(KeyCode.Return))
		{
			if(SelectedMenuItem == PlayMenuItem)
			{
				SceneManager.LoadScene("CutsceneIntermission");
			}
			else if(SelectedMenuItem == InstructionsMenuItem)
			{
				if(UICanvas.activeSelf == true)
				{
					UICanvas.SetActive(false);
					FullInstructions.SetActive(true);
				}
				else if (UICanvas.activeSelf == false)
				{
					UICanvas.SetActive(true);
					FullInstructions.SetActive(false);

				}
			}
		}
	}


	private void ToggleMenuItem()
	{
		SelectedMenuItem.GetComponent<Pulse>().enabled = false;
		if(SelectedMenuItem == PlayMenuItem)
		{
			SelectedMenuItem = InstructionsMenuItem;
		}
		else if(SelectedMenuItem == InstructionsMenuItem)
		{
			SelectedMenuItem = PlayMenuItem;
		}
		SelectedMenuItem.GetComponent<Pulse>().enabled = true;
	}
}
