﻿using UnityEngine;
using System.Collections;

public class LevelState
{

    protected LevelStateManager m_Manager;

    public LevelState()
    {

    }

    public LevelState(LevelStateManager l_Manager)
    {
        m_Manager = l_Manager;
    }

    public void Enter(Enums.LevelState p_prevState)
    {
        EnterState(p_prevState);
    }

    public void Leave(Enums.LevelState p_nextState)
    {
        LeaveState(p_nextState);
    }

    public void Update()
    {
        UpdateState();
    }

    //State control functions

    protected virtual void EnterState(Enums.LevelState prevState)
    {

    }

    protected virtual void LeaveState(Enums.LevelState nextState)
    {

    }

    protected virtual void UpdateState()
    {

    }
}
