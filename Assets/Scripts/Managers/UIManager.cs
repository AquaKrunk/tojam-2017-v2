﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour {

    /*CONTANTS*/
    /*Countdown Timings*/
    private float TIMING_3 = 0;
    private float TIMING_2 = 0;
    private float TIMING_1 = 0;
    private float TIMING_4 = 0;
    private float TIMING_ELLIPSIS = 0;
    private float TIMING_GO = 0;

    public Image sergeiBar;
    public Image yuriBar;
    public Text sergeiAlcohol;
    public Text yuriAlcohol;
    public Text timerText;

    public string[] drunkLevels;

    public Image[] sergeiBreads;
    public Image[] yuriBread;


    public static UIManager instance;
    private int countDownState = 4;
    public Text countDownTimerText;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        
    }

    public void IncrementCountDownTimer()
    {
        countDownState--;

        if (countDownState == 3)
        {
            countDownTimerText.gameObject.SetActive(true);
            countDownTimerText.text = countDownState.ToString();
            StartCoroutine(CountDownAnimation(0.5f));
        }
        else if (countDownState > 0)
        {
            countDownTimerText.text = countDownState.ToString();
            StartCoroutine(CountDownAnimation(0.5f));
        }
        else if (countDownState == 0)
        {
            countDownTimerText.text = "SQUAT!";
            StartCoroutine(CountDownAnimation(0.5f));
        }
    }

    public void ActivateCountdownTimer()
    {
        countDownTimerText.gameObject.SetActive(true);
    }

    public void ShowCountdownText(string _text, float _duration)
    {
        countDownTimerText.text = _text;
        StartCoroutine(CountDownAnimation(_duration));
    }

    IEnumerator CountDownAnimation(float p_textTime)
    {
        countDownTimerText.transform.localScale = new Vector3(5, 5, 5);

        countDownTimerText.color = new Color(countDownTimerText.color.r, countDownTimerText.color.g, countDownTimerText.color.b, 1f);

        countDownTimerText.transform.DOScale(1f, p_textTime/2);

        yield return new WaitForSeconds(p_textTime);

        countDownTimerText.DOFade(0, p_textTime/2);
        //DOTween.To(() => countDownTimerText.color, x => countDownTimerText.color = x, new Color(countDownTimerText.color.r, countDownTimerText.color.g, countDownTimerText.color.b, 0f), 0.45f);

        yield return new WaitForSeconds(0.46f);

        if (countDownState == 0)
        {
            countDownState = 4;            
            //countDownTimerText.gameObject.SetActive(false);
        }
    }

    public void UpdateDrunkLevel(float amount, string tag)
    {
        float drunkPercent = amount / 10f;

        if (tag == "Player1")
        {
            DOTween.To(() => sergeiBar.fillAmount, x => sergeiBar.fillAmount = x, drunkPercent, 0.5f);

            if (drunkPercent > 0.75f)
                sergeiAlcohol.text = drunkLevels[3];
            else if (drunkPercent > 0.5f)
                sergeiAlcohol.text = drunkLevels[2];
            else if (drunkPercent > 0.25f)
                sergeiAlcohol.text = drunkLevels[1];

        }

        if (tag == "Player2")
        {
            DOTween.To(() => yuriBar.fillAmount, x => yuriBar.fillAmount = x, drunkPercent, 0.5f);

            if (drunkPercent > 0.75f)
                yuriAlcohol.text = drunkLevels[3];
            else if (drunkPercent > 0.5f)
                yuriAlcohol.text = drunkLevels[2];
            else if (drunkPercent > 0.25f)
                yuriAlcohol.text = drunkLevels[1];
            
            
        }
    }

    public void ResetDrunkLevels()
    {
        sergeiBar.fillAmount = 0;
        yuriBar.fillAmount = 0;

        sergeiAlcohol.text = drunkLevels[0];
        yuriAlcohol.text = drunkLevels[0];
    }

    public void SetTimerText(int value)
    {
        timerText.text = value.ToString();
    }

    public void IncrementYuriScore()
    {
        foreach (Image img in yuriBread)
        {
            if (!img.gameObject.activeSelf)
            {
                img.gameObject.SetActive(true);
                break;
            }
        }
    }

    public void IncrementSergeiScore()
    {
        foreach (Image img in sergeiBreads)
        {
            if (!img.gameObject.activeSelf)
            {
                img.gameObject.SetActive(true);
                break;
            }
        }
    }
}
