﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LevelStateManager : MonoBehaviour {

    public static LevelStateManager instance;

    public Transform Ground;
    public GameObject m_player1Prefab;
    public GameObject m_player2Prefab;
    public GameObject m_car;
    public AudioSource TickSource;
    public AudioSource BellSource;


    private GameObject m_player1;
    private GameObject m_player2;

    private bool playersSpawned = false;

    private bool player1Fell = false;
    private bool player2Fell = false;
    private int roundWinner = 0;

    public int roundNumber = 1;
    public int player1Wins = 0;
    public int player2Wins = 0;

    public float roundTimer;

    private Dictionary<Enums.LevelState, LevelState> m_LevelStateDictionary;
    private LevelState m_currentState;

    private Enums.LevelState m_currentStateIndex;
    private Enums.LevelState m_nextStateIndex;

    private bool m_initialised;

    private Coroutine roundEndTimer;

    public Enums.LevelState CurrentStateIndex
    {
        get { return m_currentStateIndex; }
    }

    public LevelState CurrentState
    {
        get { return m_currentState; }
    }

    public GameObject Player1
    {
        get { return m_player1; }
    }

    public GameObject Player2
    {
        get { return m_player2; }
    }

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

    }

    // Use this for initialization
    void Start () {
        m_LevelStateDictionary = new Dictionary<Enums.LevelState, LevelState>();
        m_currentState = null;

        m_currentStateIndex = Enums.LevelState.Null;
        m_nextStateIndex = Enums.LevelState.Null;

        m_initialised = false;


        //temp
        //put this in gamestate manager
        Initialise();
        //ChangeState(Enums.PartyBoxState.Start);
    }

    // Update is called once per frame
    void Update () {
        if (!m_initialised)
            return;

        if (m_currentState != null)
            m_currentState.Update();

        if (m_nextStateIndex != Enums.LevelState.Null)
        {
            m_currentState.Leave(m_nextStateIndex);
            m_currentState = m_LevelStateDictionary[m_nextStateIndex];
            m_currentState.Enter(m_currentStateIndex);

            m_currentStateIndex = m_nextStateIndex;
            m_nextStateIndex = Enums.LevelState.Null;
        }
    }

    public void Initialise()
    {
        m_currentStateIndex = Enums.LevelState.Enter;

        m_LevelStateDictionary = new Dictionary<Enums.LevelState, LevelState>();

        m_LevelStateDictionary.Add(Enums.LevelState.Enter, new LevelState_Enter(this));
        m_LevelStateDictionary.Add(Enums.LevelState.Spawn, new LevelState_Spawn(this));
        m_LevelStateDictionary.Add(Enums.LevelState.Ready, new LevelState_Ready(this));
        m_LevelStateDictionary.Add(Enums.LevelState.Starting, new LevelState_Starting(this));
        m_LevelStateDictionary.Add(Enums.LevelState.InPlay, new LevelState_InPlay(this));
        m_LevelStateDictionary.Add(Enums.LevelState.Finish, new LevelState_Finish(this));
        m_LevelStateDictionary.Add(Enums.LevelState.Leave, new LevelState_Leave(this));
        m_LevelStateDictionary.Add(Enums.LevelState.RoundEnd, new LevelState_RoundEnd(this));
        m_LevelStateDictionary.Add(Enums.LevelState.RoundStarting, new LevelState_RoundStarting(this));
        m_LevelStateDictionary.Add(Enums.LevelState.GameOver, new LevelState_GameOver(this));

        //start game at movement state
        //ChangeTutorialState(Enums.TutorialState.Enter);

        m_currentState = m_LevelStateDictionary[(Enums.LevelState)m_currentStateIndex];

        m_currentState.Enter(m_currentStateIndex);

        //INIT PARTY MEMBERS HERE
        //
        //
        //
        //

        m_initialised = true;
    }

    public void Destroy()
    {
        m_LevelStateDictionary = new Dictionary<Enums.LevelState, LevelState>();

        m_currentStateIndex = Enums.LevelState.Null;

        m_currentState = null;

        m_initialised = false;
    }

    public void ChangeState(Enums.LevelState l_nextState)
    {
        if (!m_LevelStateDictionary.ContainsKey(l_nextState))
            return;

        m_nextStateIndex = l_nextState;
    }

    public void BeginCountDownAnimation()
    {
        StartCoroutine(CountDownAnimation(UIManager.instance));
    }

    IEnumerator CountDownAnimation(UIManager p_uiManager)
    {
        //yield return new WaitForSeconds(1f);

        p_uiManager.ShowCountdownText("Round " + roundNumber, 1f);

        yield return new WaitForSeconds(2f);

        UIManager.instance.ResetDrunkLevels();

        p_uiManager.ActivateCountdownTimer();
        p_uiManager.ShowCountdownText("SQ-!", 0.5f);
        TickSource.Play();

        yield return new WaitForSeconds(1f);

        p_uiManager.ActivateCountdownTimer();
        p_uiManager.ShowCountdownText("SQ-!", 0.5f);
        TickSource.Play();

        yield return new WaitForSeconds(1f);

        p_uiManager.ActivateCountdownTimer();
        p_uiManager.ShowCountdownText("SQ-!", 0.5f);
        TickSource.Play();

        yield return new WaitForSeconds(1f);

        p_uiManager.ActivateCountdownTimer();
        p_uiManager.ShowCountdownText("SQUAT!", 0.4f);
        BellSource.Play();

        ChangeState(Enums.LevelState.InPlay);

        /*
        yield return new WaitForSeconds(1f);

        p_uiManager.IncrementCountDownTimer();

        yield return new WaitForSeconds(1f);

        p_uiManager.IncrementCountDownTimer();

        yield return new WaitForSeconds(1f);

        p_uiManager.IncrementCountDownTimer();

        yield return new WaitForSeconds(1f);

        p_uiManager.IncrementCountDownTimer();
        ChangeState(Enums.LevelState.InPlay);
        */
    }
    
    public void BeginFirstCountDownAnimation()
    {
        StartCoroutine(FirstCountDownAnimation(UIManager.instance));
    }

    IEnumerator FirstCountDownAnimation(UIManager p_uiManager)
    {
        yield return new WaitForSeconds(0.5f);
        p_uiManager.ActivateCountdownTimer();
        p_uiManager.ShowCountdownText("5", 0.5f);
        yield return new WaitForSeconds(1.6f);
        p_uiManager.ShowCountdownText("4", 0.5f);
        yield return new WaitForSeconds(1f);
        p_uiManager.ShowCountdownText("3", 0.16f);
        yield return new WaitForSeconds(0.25f);
        p_uiManager.ShowCountdownText("2", 0.25f);
        yield return new WaitForSeconds(1f);
        p_uiManager.ShowCountdownText("...", 0.5f);
        yield return new WaitForSeconds(1.3f);
        p_uiManager.ShowCountdownText("1", 0.5f);
        yield return new WaitForSeconds(2.7f);
        p_uiManager.ShowCountdownText("SQUAT", 0.4f);

        ChangeState(Enums.LevelState.InPlay);
    }

    public void BeginRoundEndAnimation()
    {
        StartCoroutine(RoundEndAnimation(UIManager.instance));
    }

    IEnumerator RoundEndAnimation(UIManager p_uiManager)
    {
        if (roundWinner == 1)
            p_uiManager.ShowCountdownText("Sergei Wins", 2.5f);
        else if (roundWinner == 2)
            p_uiManager.ShowCountdownText("Yuri Wins", 2.5f);
        else if (roundWinner == 0)
            p_uiManager.ShowCountdownText("Draw", 2.5f);
        else if (roundWinner == -1)
            p_uiManager.ShowCountdownText("Time Up", 2.5f);

        yield return new WaitForSeconds(4f);

        ChangeState(Enums.LevelState.RoundStarting);
    }

    public void BeginGameOverAnimation()
    {
        StartCoroutine(GameOverAnimation(UIManager.instance));
    }

    IEnumerator GameOverAnimation(UIManager p_uiManager)
    {
        p_uiManager.ShowCountdownText("Game Over", 2.5f);

        yield return new WaitForSeconds(4f);

        if (player1Wins == 3)
        {
            p_uiManager.ShowCountdownText("Sergei Wins", 2.5f);
            GameObject l_car = GameObject.Instantiate(m_car);
            l_car.transform.position = new Vector3(m_player1.transform.position.x, l_car.transform.position.y, l_car.transform.position.z);
        }
        else
        {
            p_uiManager.ShowCountdownText("Yuri Wins", 2.5f);
            GameObject l_car = GameObject.Instantiate(m_car);
            l_car.transform.position = new Vector3(m_player2.transform.position.x, l_car.transform.position.y, l_car.transform.position.z);
        }

        yield return new WaitForSeconds(10f);

        ChangeState(Enums.LevelState.Leave);
    }

    public void SpawnPlayers()
    {
        if (!playersSpawned)
        {
            m_player1 = (GameObject)Instantiate(m_player1Prefab);
            m_player2 = (GameObject)Instantiate(m_player2Prefab);

            playersSpawned = true;
        }
    }

    public void DespawnPlayers()
    {
        if (playersSpawned)
        {
            Destroy(m_player1);
            Destroy(m_player2);

            playersSpawned = false;
        }
    }

    public void ActivatePlayerRigs()
    {
        if (playersSpawned)
        {
            m_player1.GetComponent<SquatterController>().UnlockRig();
            m_player2.GetComponent<SquatterController>().UnlockRig();
        }
    }

    public void RoundEnd(int playerNumber)
    {
        if (m_currentStateIndex == Enums.LevelState.InPlay)
        {

            if (playerNumber == 1)
            {
                player1Fell = true;
            }
            else if (playerNumber == 2)
            {
                player2Fell = true;
            }

            if (roundEndTimer == null)
                roundEndTimer = StartCoroutine(RoundEndTimer());
        }
    }

    IEnumerator RoundEndTimer()
    {
        yield return new WaitForSeconds(0.1f);

        if (player1Fell && !player2Fell)
        {
            roundWinner = 2;
            player2Wins++;
            UIManager.instance.IncrementYuriScore();
        }
        else if (!player1Fell && player2Fell)
        {
            roundWinner = 1;
            player1Wins++;
            UIManager.instance.IncrementSergeiScore();
        }
        else
        {
            roundWinner = 0;    //Draw
        }

        player1Fell = false;
        player2Fell = false;

        Debug.Log(roundWinner);

        if (player1Wins == 3 || player2Wins == 3)
        {
            ChangeState(Enums.LevelState.GameOver);
        }
        else
        {
            roundNumber++;

            ChangeState(Enums.LevelState.RoundEnd);
        }

        roundEndTimer = null;
    }

    public void OutOfTime()
    {
        roundWinner = -1;

        player1Fell = false;
        player2Fell = false;

        roundNumber++;

        ChangeState(Enums.LevelState.RoundEnd);
    }
}
