﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour {

    public static InputManager instance;

    /*VARIABLES*/
    private SquatterController Player1Control;
	private SquatterController Player2Control;

    private float Player1ChargeTime = 0;
    private float Player2ChargeTime = 0;

    private bool Player1Charging = false;
    private bool Player2Charging = false;

    private bool initialized = false;
    public bool ControlLock = true;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

    }

    // Use this for initialization
    void Start () {
		//Player1Control = GameObject.Find("Player_One").GetComponent<SquatterController>();
		//Player2Control = GameObject.Find("Player_Two").GetComponent<SquatterController>();
	}
	
	// Update is called once per frame
	void Update () {

        if (initialized)
        {
            CheckLeftPlayerInputs();
            CheckRightPlayerInputs();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void InitializeControls()
    {
        if(SceneManager.GetActiveScene().name == "MainMenu")
        {
            Player1Control = GameObject.Find("Player_One").GetComponent<SquatterController>();
            Player2Control = GameObject.Find("Player_Two").GetComponent<SquatterController>();
        }
        else
        {
            Player1Control = LevelStateManager.instance.Player1.GetComponent<SquatterController>();
            Player2Control = LevelStateManager.instance.Player2.GetComponent<SquatterController>();
        }
        initialized = true;
    }

    public void UnhookControls()
    {
        Player1Control = null;
        Player2Control = null;
        initialized = false;
    }

    private void CheckLeftPlayerInputs()
	{
		//Left Leg Inputs
		if(Input.GetKeyDown("a"))
		{
			Player1Control.ActivateLeftLeg();
		}
		else if(Input.GetKeyUp("a"))
		{
            Player1Control.DeactivateLeftLeg();
		}
		//Right Leg Inputs
		if(Input.GetKeyDown("d"))
		{
            Player1Control.ActivateRightLeg();
		}
		else if(Input.GetKeyUp("d"))
		{
            Player1Control.DeactivateRightLeg();
		}
        /*
		//Jump Input
		if(Input.GetKeyDown("w"))
		{
            if (!ControlLock)
                Player1Control.Jump();
		}
        */

        if (!ControlLock)
        {
            if (Input.GetKeyDown("s"))
            {
                if (!ControlLock)
                    Player1Control.ChargeBottle();
            }

            //Throw Input
            if (Input.GetKey("s") && Player1ChargeTime < 1.5f)
            {
                Player1ChargeTime += Time.deltaTime;
            }
            else if (Input.GetKeyUp("s"))
            {
                Player1Control.CompleteChargeBottle(Player1ChargeTime, new Vector3(1, 0, 0));
                Player1ChargeTime = 0f;
            }
        }

    }

	private void CheckRightPlayerInputs()
	{
		//Left Leg Inputs
		if(Input.GetKeyDown(KeyCode.LeftArrow))
		{
            Player2Control.ActivateLeftLeg();
		}
		else if(Input.GetKeyUp(KeyCode.LeftArrow))
		{
            Player2Control.DeactivateLeftLeg();
		}
		//Right Leg Inputs(
		if(Input.GetKeyDown(KeyCode.RightArrow))
		{
            Player2Control.ActivateRightLeg();
		}
		else if(Input.GetKeyUp(KeyCode.RightArrow))
		{
            Player2Control.DeactivateRightLeg();
		}
        /*
		//Jump Input
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
            if (!ControlLock)
                Player2Control.Jump();
		}
        */

        if (!ControlLock)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Player2Control.ChargeBottle();
            }

            //Throw Input
            if (Input.GetKey(KeyCode.DownArrow) && Player2ChargeTime < 1.5f)
            {
                Player2ChargeTime += Time.deltaTime;
            }
            else if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                Player2Control.CompleteChargeBottle(Player2ChargeTime, new Vector3(-1, 0, 0));
                Player2ChargeTime = 0f;
            }
        }

    }
}
