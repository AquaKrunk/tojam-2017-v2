﻿using UnityEngine;
using System.Collections;

public class LevelState_RoundStarting : LevelState
{

    public LevelState_RoundStarting(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("round starting state");

        m_Manager.SpawnPlayers();

        InputManager.instance.ControlLock = true;

        InputManager.instance.InitializeControls();

        UIManager.instance.ResetDrunkLevels();

        UIManager.instance.SetTimerText(30);

        m_Manager.BeginCountDownAnimation();
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {

    }

    protected override void UpdateState()
    {

    }
}