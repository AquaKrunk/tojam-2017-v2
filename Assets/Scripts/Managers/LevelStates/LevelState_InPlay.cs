﻿using UnityEngine;
using System.Collections;

public class LevelState_InPlay : LevelState
{
    int nextThreshold = 29;

    public LevelState_InPlay(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("play state");

        LevelStateManager.instance.ActivatePlayerRigs();

        InputManager.instance.ControlLock = false;

        m_Manager.roundTimer = 30f;

        nextThreshold = 29;
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {

    }

    protected override void UpdateState()
    {
        m_Manager.roundTimer -= Time.deltaTime;

        if (m_Manager.roundTimer <= nextThreshold)
        {
            UIManager.instance.SetTimerText(nextThreshold);
            nextThreshold--;
        }

        if (m_Manager.roundTimer <= 0)
        {
            m_Manager.OutOfTime();
        }
    }
}