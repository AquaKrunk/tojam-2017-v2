﻿using UnityEngine;
using System.Collections;

public class LevelState_Enter : LevelState
{

    public LevelState_Enter(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("enter state");
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {

    }

    protected override void UpdateState()
    {
        m_Manager.ChangeState(Enums.LevelState.Spawn);
    }
}