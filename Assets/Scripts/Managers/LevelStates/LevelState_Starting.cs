﻿using UnityEngine;
using System.Collections;

public class LevelState_Starting : LevelState
{

    public LevelState_Starting(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("start state");

        m_Manager.BeginFirstCountDownAnimation();
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {

    }

    protected override void UpdateState()
    {

    }
}