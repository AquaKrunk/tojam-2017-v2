﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelState_Leave : LevelState
{

    public LevelState_Leave(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("leave state");

        SceneManager.LoadScene("MainMenu");
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {

    }

    protected override void UpdateState()
    {

    }
}