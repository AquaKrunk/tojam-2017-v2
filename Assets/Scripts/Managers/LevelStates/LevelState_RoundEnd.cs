﻿using UnityEngine;
using System.Collections;

public class LevelState_RoundEnd : LevelState
{

    public LevelState_RoundEnd(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("round end state");

        m_Manager.BeginRoundEndAnimation();
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {
        InputManager.instance.UnhookControls();

        m_Manager.DespawnPlayers();
    }

    protected override void UpdateState()
    {

    }
}