﻿using UnityEngine;
using System.Collections;

public class LevelState_Ready : LevelState
{

    public LevelState_Ready(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("ready state");
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {

    }

    protected override void UpdateState()
    {
        m_Manager.ChangeState(Enums.LevelState.Starting);
    }
}