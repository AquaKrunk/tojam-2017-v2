﻿using UnityEngine;
using System.Collections;

public class LevelState_GameOver : LevelState
{

    public LevelState_GameOver(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("game over state");

        m_Manager.BeginGameOverAnimation();
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {

    }

    protected override void UpdateState()
    {

    }
}