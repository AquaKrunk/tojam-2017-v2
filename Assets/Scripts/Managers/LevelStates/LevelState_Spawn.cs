﻿using UnityEngine;
using System.Collections;

public class LevelState_Spawn : LevelState
{

    public LevelState_Spawn(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("spawn state");

        BottlePool.instance.InitializeBottlePool(20);

        m_Manager.SpawnPlayers();

        InputManager.instance.InitializeControls();
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {

    }

    protected override void UpdateState()
    {
        m_Manager.ChangeState(Enums.LevelState.Ready);
    }
}