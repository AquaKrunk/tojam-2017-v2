﻿using UnityEngine;
using System.Collections;

public class LevelState_Finish : LevelState
{

    public LevelState_Finish(LevelStateManager p_Manager)
        : base(p_Manager)
    {

    }

    protected override void EnterState(Enums.LevelState p_prevState)
    {
        Debug.Log("finish state");

        m_Manager.ChangeState(Enums.LevelState.Spawn);
    }

    protected override void LeaveState(Enums.LevelState p_nextState)
    {

    }

    protected override void UpdateState()
    {

    }
}