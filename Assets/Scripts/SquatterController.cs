﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SquatterController : MonoBehaviour {

    /*Constants*/
    private float SWAY_MULTIPLIER = 1;
    int THROW_FORCE_MULTIPLIER = 600;

    /*Scene Object References*/
    //public GameObject Bottle;
    public HingeJoint2D leftThigh;
    public HingeJoint2D rightThigh;
    public Rigidbody2D Head;
    public Canvas BottleUICanvas;
    public Image BottleUI;
    public float MeterCharge = 0;

    public Animator headAnimator;
    public Animator torsoAnimator;
    public Animator handAnimator;

    public Collider2D[] ignoreFloorColliders;
    public Collider2D[] ignoreTorsoColliders;

    private Collider2D floorCollider;

    /*Variables*/
    public float DrunkLevel;
    private float SinWaveX;
    private float bottleCD = 0f;
    private float bottleCDTime = 3f;
    private bool hasBottle = true;
    private bool IsCharging = false;
    private bool rigLocked = true;

    private float m_chargeTime;
    private Vector3 m_throwDirection;

    public Transform Torso;
	// Use this for initialization
	void Start () {
        DrunkLevel = 0;
        SinWaveX = 0;

        if(SceneManager.GetActiveScene().name == "MainMenu")
        {
            
        }
        else
        {
            floorCollider = LevelStateManager.instance.Ground.GetComponent<Collider2D>();
        }


        foreach (Collider2D col in ignoreFloorColliders)
        {
            Physics2D.IgnoreCollision(col, floorCollider);
        }

        /*
        foreach (Collider2D col in ignoreTorsoColliders)
        {
            Physics2D.IgnoreCollision(col, Torso.GetComponent<Collider2D>());
        }
        */

        if (hasBottle)
        {
            torsoAnimator.SetBool("HasBottle", true);
            bottleCD = bottleCDTime;
        }

        //LockRig();
    }

	// Update is called once per frame
	void Update () {
		ApplyDrunkenEffects();

        if (!hasBottle)
        {
            bottleCD -= Time.deltaTime;

            if (bottleCD <= 0)
            {
                hasBottle = true;
                torsoAnimator.SetBool("HasBottle", true);
                bottleCD = bottleCDTime;
            }
        }
        UpdatePlayerUI();
	}

    public void ActivateRightLeg()
    {
        rightThigh.useMotor = true;
    }

    public void DeactivateRightLeg()
    {
        rightThigh.useMotor = false;
    }

    public void ActivateLeftLeg()
    {
        leftThigh.useMotor = true;
    }

    public void DeactivateLeftLeg()
    {
        leftThigh.useMotor = false;
    }

    public void Jump()
    {
        //Torso.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;
        Head.AddForce(7500*this.transform.up);
    }

    public void ChargeBottle()
    {
        if (hasBottle)
        {
            MeterCharge = 0;
            IsCharging = true;
            torsoAnimator.SetBool("IsThrowing", true);
            handAnimator.SetBool("IsThrowing", true);
            headAnimator.SetBool("IsThrowing", true);
        }
    }

    public void CompleteChargeBottle(float chargeTime, Vector3 direction)
    {
        IsCharging = false;
        m_chargeTime = chargeTime;
        m_throwDirection = direction;
        torsoAnimator.SetBool("IsThrowing", false);
        handAnimator.SetBool("IsThrowing", false);
        headAnimator.SetBool("IsThrowing", false);
    }

    public void ThrowBottle()
    {
        if (hasBottle)
        {
            GameObject newBottle = BottlePool.instance.GetBottle();
            newBottle.transform.position = Torso.position + 1.6f * Torso.up + m_throwDirection.x * 1.6f * Torso.right;
            newBottle.SetActive(true);
            newBottle.GetComponent<Rigidbody2D>().AddForce((m_throwDirection.x * Torso.right + Torso.up*0.3f) * m_chargeTime * THROW_FORCE_MULTIPLIER);
            newBottle.GetComponent<Rigidbody2D>().AddTorque(-100 * (m_throwDirection.x / Mathf.Abs(m_throwDirection.x)));
            DrunkLevel += 1.5f * m_chargeTime;
            if (UIManager.instance != null)
                UIManager.instance.UpdateDrunkLevel(DrunkLevel, transform.tag);

            hasBottle = false;
            torsoAnimator.SetBool("HasBottle", false);
        }

    }
    
    private void ApplyDrunkenEffects()
    {
        SinWaveX += Time.deltaTime;
        Head.AddForce(3* Mathf.Sin(SinWaveX)*DrunkLevel*Mathf.Sin(SinWaveX) * Head.transform.right);

    }

    public void LockRig()
    {
        if (!rigLocked)
        {
            foreach (Transform child in transform)
            {
                Rigidbody2D rb = child.GetComponentInChildren<Rigidbody2D>();

                if (rb != null)
                    rb.freezeRotation = true;
            }

            rigLocked = true;
        }
    }

    public void UnlockRig()
    {
        if (rigLocked)
        {
            foreach (Transform child in transform)
            {
                Rigidbody2D rb = child.GetComponentInChildren<Rigidbody2D>();

                if (rb != null)
                    rb.freezeRotation = false;
            }

            rigLocked = false;
        }
    }

    private void UpdatePlayerUI()
    {
        if(IsCharging)
        {
            MeterCharge += Time.deltaTime;
            BottleUICanvas.enabled = true;
            BottleUI.fillAmount = MeterCharge/1.5f;
        }
        else
        {
            BottleUICanvas.enabled = false;
        }
    }
}
