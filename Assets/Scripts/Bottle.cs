﻿using UnityEngine;
using System.Collections;

public class Bottle : MonoBehaviour
{

    public AudioSource BottleFlight;
    public AudioSource BottleImpact;

    private BottlePool m_bulletManager;
    private SpriteRenderer m_sprite;
    protected float m_TTLTimer = 5f;

    //Unity default
    public virtual void Start()
    {

    }

    public virtual void Update()
    {
        if (m_TTLTimer > 0)
        {
            m_TTLTimer -= Time.deltaTime;

            if (m_TTLTimer <= 0)
            {
                DestroyBullet();
            }
        }
    }

    protected void DestroyBullet()
    {
        m_TTLTimer = 5f;
        m_bulletManager.DestroyObjectPool(gameObject);
    }

    public void SetBulletManager(BottlePool p_bulletManager)
    {
        m_bulletManager = p_bulletManager;
        m_sprite = GetComponent<SpriteRenderer>();
    }

    public virtual void ActivateBullet()
    {
        gameObject.SetActive(true);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        BottleFlight.enabled = false;
        BottleImpact.enabled = true;
    }
}
