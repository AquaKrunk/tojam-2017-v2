﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pulse : MonoBehaviour {

	private float OriginalFontSize;
	private float ElapsedTime;

	// Use this for initialization
	void Start () {
		OriginalFontSize = this.GetComponent<Text>().fontSize;
		ElapsedTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		ElapsedTime += 10 * Time.deltaTime;
		this.GetComponent<Text>().fontSize = (int)(OriginalFontSize + 6 * Mathf.Sin(ElapsedTime));	
	}

	void OnDisable()
	{
		this.GetComponent<Text>().fontSize = 70;
	}
}
