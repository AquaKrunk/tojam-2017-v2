﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleThrower : MonoBehaviour {

    private SquatterController squatterController;

    private void Start()
    {
        squatterController = transform.parent.GetComponent<SquatterController>();
    }

    public void ThrowBottle()
    {
        squatterController.ThrowBottle();
    }

}
