﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorsoCollision : MonoBehaviour {

    public int playerNumber;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnCollisionEnter2D(Collision2D collision)
    {
		if(collision.collider.gameObject.name == "ground")
		{
            LevelStateManager.instance.RoundEnd(playerNumber);
		}
    }
}
