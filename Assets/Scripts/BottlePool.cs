﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BottlePool : MonoBehaviour
{
    public static BottlePool instance;

    private List<GameObject> m_bulletList;
    public GameObject m_bulletPrefab;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public GameObject GetBottle()
    {
        if (m_bulletList.Count > 0)
        {
            GameObject obj = m_bulletList[0];
            m_bulletList.RemoveAt(0);
            return obj;
        }
        else
        {
            GameObject obj = (GameObject)Instantiate(m_bulletPrefab);
            obj.GetComponent<Bottle>().SetBulletManager(this);
            return obj;
        }
    }

    public void DestroyObjectPool(GameObject obj)
    {
        m_bulletList.Add(obj);
        obj.SetActive(false);
    }

    public void ClearPool()
    {
        for (int i = m_bulletList.Count - 1; i > 0; i--)
        {
            GameObject obj = m_bulletList[i];
            m_bulletList.RemoveAt(i);
            Destroy(obj);
        }

        m_bulletList = null;
    }

    public GameObject GetPrefabType()
    {
        return m_bulletPrefab;
    }

    public void InitializeBottlePool(int size)
    {
        m_bulletList = new List<GameObject>();

        for (int i = 0; i < size; i++)
        {
            GameObject obj = (GameObject)Instantiate(m_bulletPrefab);
            obj.GetComponent<Bottle>().SetBulletManager(this);
            obj.SetActive(false);
            m_bulletList.Add(obj);
        }
    }
}
