﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums : MonoBehaviour {

    public enum LevelState
    {
        Null = -1,
        Enter = 0,
        Spawn = 1,
        Ready = 2,
        Starting = 3,
        InPlay = 4,
        Finish = 5,
        Leave = 6,
        RoundEnd = 7,
        RoundStarting = 8,
        GameOver = 9
    }
}
